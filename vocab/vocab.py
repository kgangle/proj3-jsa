"""
A list of vocabulary words (designed for an ELL class).
"""

import flask
import logging
import config


###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY  # Sign my cookies


###
# Pages
###

@app.route("/")
def index():
    return flask.render_template('vocab.html')

###############
# AJAX request handlers
#   These return JSON to the JavaScript function on
#   an existing page, rather than rendering a new page.
###############


@app.route("/_matches")
def matches():
    text = flask.request.args.get("text", type=str)
    
    rslt = {"long_enough": length >= 5}
    return flask.jsonify(result=rslt)

#############


if __name__ == "__main__":
    # Standalone (not running under green unicorn or similar)
    if CONFIG.DEBUG:
        app.debug = True
        app.logger.setLevel(logging.DEBUG)
    app.logger.info("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")




class Vocab():
    """
    A list of vocabularly words.
    Can be instantiated with a file or list of strings.
    """

    def __init__(self, wordlist):
        """
        Initialize with the provided word list.
        Args:
           wordlist: a file, path to a file, or a list of strings.
           Words must appear one to a line. Empty lines and lines
           beginning with # are treated as comments.
        Returns: nothing
        Effect: the new Vocab objects contains the strings from wordlist
        Raises:  IOError if if wordlist is a bad path
        """
        self.words = []
        if isinstance(wordlist, str):
            ls = open(wordlist, 'r')
        else:
            ls = wordlist

        for word in ls:
            word = word.strip()
            if len(word) == 0 or word.startswith("#"):
                continue
            self.words.append(word)
        self.words.sort()

    def as_list(self):
        """As list of words"""
        return self.words

    def has(self, word):
        """
        Is word present in vocabulary list?
        Args:
           word: a string
        Reurns: true if word occurs in the vocabularly list
        """
        low = 0
        high = len(self.words) - 1
        while low <= high:
            mid = (low + high) // 2
            probe = self.words[mid]
            if word > probe:
                low = mid + 1
            elif word < probe:
                high = mid - 1
            else:
                return True
       return False
     
